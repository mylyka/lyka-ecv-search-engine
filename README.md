## Lyka search API

Restful API for searching users, merchants, places and hashtags

## Swagger specification

[Swagger specification file](lyka-search-swagger-specification.yaml)

## Serverless Deployment

### Preparation
1. Create an ElasticSearch cluster
2. Create an ElasticCache Redis cluster

1. Make sure AWS CLI and Serverless Framework is installed and configured
2. Create a Python Virtual Environment
```
# Install pyenv

# Install desired Python version using pyenv
pyenv install 3.8.6

# Create a venv with pyenv
pyenv virtualenv 3.8.6 python3.8.6

Activate env on project directory
pyenv local python3.8.6
pyenv activate
```

3. Install the required dependencies to the exact directory, this is used by Serverless to create a  lambda layer
```
pip install -r requirements.txt -t ./lib/python
```

4. Deploy using Serverless
```
serverless deploy --config serverless.yml --region ap-southeast-1
```
#!/bin/bash
# Login to get bearer token, saved as env accessToken
#newman run lyka-search-deployment.postman_collection.json --environment AWS_UAT_PG.postman_environment.json --folder "Login" --export-environment AWS_UAT_PG.postman_environment.json

# Run test for Search Users endpoint
newman run lyka-search-deployment.postman_collection.json --environment AWS_UAT_PG.postman_environment.json --folder "Test search users" -d test_data/search_user_data.json

# Run test for Search Merchants endpoint
newman run lyka-search-deployment.postman_collection.json --environment AWS_UAT_PG.postman_environment.json --folder "Test search merchants" -d test_data/search_merchant_data.json

# Run test for Search Hashtags endpoint
newman run lyka-search-deployment.postman_collection.json --environment AWS_UAT_PG.postman_environment.json --folder "Test search hashtags" -d test_data/search_hashtag_data.json

# Run test for Search Places endpoint
newman run lyka-search-deployment.postman_collection.json --environment AWS_UAT_PG.postman_environment.json --folder "Test search places" -d test_data/search_places_data.json

# Run test for inserting new post (post and hashtag index)
newman run lyka-search-deployment.postman_collection.json --environment AWS_UAT_PG.postman_environment.json --folder "Test correct post entry created" -d test_data/insert_post_data.json --delay-request 2000

# Run test for inserting new users
newman run lyka-search-deployment.postman_collection.json --environment AWS_UAT_PG.postman_environment.json --folder "Test correct user entry created" -d test_data/insert_user_data.json --delay-request 2000


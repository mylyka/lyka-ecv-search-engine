import os
import traceback
def remap_url_domain(url):

    domain_mappings = {
        "https://lykaprod.blob.core.windows.net":"https://d275kx3vfow7ea.cloudfront.net",
        "https://d2midxqtbploky.cloudfront.net": "https://d275kx3vfow7ea.cloudfront.net",
        "https://lykateststorage.blob.core.windows.net": "https://lykatestcd.azureedge.net",
        "https://lykaassets.blob.core.windows.net": "https://d2y0c1sg6eq46o.cloudfront.net",
        "https://lyka.blob.core.windows.net": "https://d2y0c1sg6eq46o.cloudfront.net",
        "https://d2wmojqwcmrlfl.cloudfront.net": "https://d2y0c1sg6eq46o.cloudfront.net",
        "https://tilproduction.blob.core.windows.net": "https://d3uw8sf1j292mv.cloudfront.net",
        "https://d1lepqttuazf4i.cloudfront.net": "https://d3uw8sf1j292mv.cloudfront.net",
        "https://d3vnwcnye296sg.cloudfront.net": "https://dh1u1gri6oel1.cloudfront.net"
    }

    for old_domain in domain_mappings.keys():
        if old_domain in url:
            url = url.replace(old_domain, domain_mappings[old_domain])

    return url


def url_builder(directory, file):
    if(not directory or not file):
        return None
    try:

        new_directory = remap_url_domain(directory)
    
        url = os.path.join(new_directory, 'resized' if '/resized' not in new_directory else '', file)
    
        return url
    except:
        traceback.print_exc()
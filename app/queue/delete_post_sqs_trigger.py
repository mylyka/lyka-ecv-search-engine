import os
import json
import boto3
from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk


config = None
es_index = 'post'

def proxy_response(status, message):
   return {
            'statusCode': status,
            'headers':  {
                            'Content-Type': 'application/json',
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods': '*', 
                            'Access-Control-Allow-Headers': '*' 
                        },  
            'body': message,
            'isBase64Encoded': False
          }

def data_sequence(data_list):
    for data in data_list:
        yield data
        
def lambda_handler(event, context):
    
    print(event)
    
    global config
    if(not config):
        config = get_creds()
    
    try:
        postList = []
        for record in event['Records']:
            postList.extend(json.loads(record['body'])['data'])
    except Exception as e:
        print(e)
        raise e
    
    # ISO time in UTC 
    timestamp = datetime.utcnow().isoformat()[:-3]+'Z';
    
    client = Elasticsearch([config['es_endpoint']], port=9200, http_auth=(config['es_username'], config['es_password']))
    esPostEntry = []
    
    for post in postList:
            
        data = {
            "_id": post['id'],
            "_op_type": 'update',
            "doc": { 'deleted_at' : timestamp}
        }
        esPostEntry.append(data)

        query_dsl = {
            "script" : f"ctx._source.deleted_at = '{timestamp}'",
            "query": {
                "term": {
                    "postid": {
                        "value": post['id']
                    }
                }
            }
        }

        client.update_by_query(
            index = 'hashtag',
            body = query_dsl
        )

    
    try:
        for i,j in streaming_bulk(
                        client=client,
                        index=es_index,
                        actions=data_sequence(esPostEntry)
                      ):
            print(i, j)
            
        

        return proxy_response(200, 'PASS')
    except Exception as e:
        import sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(str(e) + "Exceptions" + str(exc_type) + str(fname) + str(exc_tb.tb_lineno))
        return proxy_response(500, 'FAIL')
        

def get_creds():
    ssm_client = boto3.client('ssm')
    try:
        params = ssm_client.get_parameters(
            Names = [os.environ['APP_CONFIG_PATH']],
            WithDecryption = True
        )
        creds = json.loads(params['Parameters'][0]['Value'])
        return creds
    except:
        print("Encountered error trying to get config from SSM")
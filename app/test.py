import os
import requests
import psycopg2
import redis
import boto3
import json

config = None

def lambda_handler(event, context):
    
    global config
    if(config):
        print("cache hit!")
        print(config)
    else:
        print("cache miss!")
        config = test_get_parameters()
        
    test_sql_connection()
    test_redis_connection()
    test_elasticsearch_connection()
    test_sqs_connection()
    
    
    

    
    
    
    
def test_sql_connection():
    server = config['sql_connection']
    database = config['sql_db_name']
    username = config['sql_id']
    password = config['sql_pw']
    cnxn = psycopg2.connect(user = username,
                            password = password,
                            host = server,
                            port = "5432",
                            database = database
                            )
    cursor = cnxn.cursor()
    
    sqlquery = 'SELECT COUNT(*) FROM "user"'
    cursor.execute(sqlquery)
    print(cursor.fetchall())

def test_redis_connection():
    redisInstance = redis.Redis(host=config['redis_endpoint'], port=6379, db=0, charset='utf-8', decode_responses=True)
    redisInstance.lpush("testing", 123)
    redisInstance.expire("testing", 20)
    
    if(redisInstance.exists("testing")):
        print("Successfully connect to Redis")
    else:
        print("Error connecting to Redis")


def test_elasticsearch_connection():
    host = config['es_endpoint']
    headers = { "Content-Type": "application/json" }
    esauth = (config['es_username'], config['es_password'])
    r = requests.get(host, auth=esauth, headers=headers).json()
    print(r)
    

def test_sqs_connection():
    sqs = boto3.Session()
    sqs_client = sqs.client(
        service_name = 'sqs',
        endpoint_url = 'https://vpce-0a95cf169fba0b75e-1sryrlxz.sqs.ap-southeast-1.vpce.amazonaws.com'
    )
    
    # Send to SQS
    response = sqs_client.send_message(
        QueueUrl = 'https://sqs.ap-southeast-1.amazonaws.com/842899535609/Post_Ingestion_ElasticSearch',
        MessageBody = "Hello there I am a test message!"
    )
    
    print(response)
    
def test_get_parameters():
    ssm_client = boto3.client('ssm')
    try:
        params = ssm_client.get_parameters(
            Names = [os.environ["APP_CONFIG_PATH"]],
            WithDecryption = True
        )
        creds = json.loads(params['Parameters'][0]['Value'])
        print(creds)
        return creds
    except:
        print("Encountered error trying to get config from SSM")
    



    

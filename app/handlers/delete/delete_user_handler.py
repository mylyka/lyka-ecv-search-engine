import json
import os
import boto3

sqs = boto3.Session()

checklist = [
    "created_at",
    "city",
    "userName",
    "country",
    "street_address",
    "id",
    "isMerchant",
    "state",
    "isVerified",
    "description",
    "location",
    "google_place_id",
    "@timestamp",
    "fullName",
    "fbId",
    "updated_at",
    "deleted_at",
    "latitude",
    "longitude",
    "directory",
    "filename"
    ]
    
required_keys = set([
    "id"
    ])
    
    
def proxy_response(status, message):
   return {
            'statusCode': status,
            'headers':  {
                            'Content-Type': 'application/json',
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods': '*', 
                            'Access-Control-Allow-Headers': '*' 
                        },  
            'body': message,
            'isBase64Encoded': False
          }

def lambda_handler(event, context):
    
    message = json.loads(event['body'])
    
    # Data validation
    for item in message['data']:
        if not set(item.keys()).issuperset(required_keys):
            return proxy_response(400, "Required field missing")
        
        for required_key in required_keys:
            if item[required_key] in [None, ""]:
                return proxy_response(400, "Required field empty")
                
        for key in item.keys():
            if key not in checklist:
                print("field unmatched : " + str(key))
                return proxy_response(400, 'Contains invalid field')

    sqs_client = sqs.client(
        service_name = 'sqs',
        #endpoint_url = os.environ["SQS_VPC_INTERFACE_ENDPOINT"]
    )
    
    # Send to SQS
    response = sqs_client.send_message(
        QueueUrl = os.environ["SQS_QUEUE_URL"],
        MessageBody = json.dumps(message)
    )
    
    return proxy_response(200, "SUCCESS")
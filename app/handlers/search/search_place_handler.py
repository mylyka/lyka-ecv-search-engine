import os
import boto3
import json
import requests
import psycopg2
import redis
from app.util.url_utils import url_builder

# Store related backend credentials with execution context
config = None
# Postgresql connection
cnxn = None
cursor = None
# Redis connection
redisInstance = None

# Lambda execution starts here
def lambda_handler(event, context):

    print(event)
    
    # Configure connections if does not exist
    init_conn()

    try:

        currentUserId = None
        
        if 'headers' in event and 'Authorization' in event['headers']:
            currentUserId = getUserId(event['headers']['Authorization'])
        
        if currentUserId is None:
            response = {
                "statusCode": 401,
                "headers": {
                    "Access-Control-Allow-Origin": '*'
                },
                "isBase64Encoded": False
            }
            return response
        
        # Default pagination values
        pageSize = 10
        pageIndex = 0
        searchText = '*'
        currentLatitude = None
        currentLongitude = None
        
        # Building query according to query string params
        if 'queryStringParameters' in event and event['queryStringParameters'] is not None:
            
            try:

                if 'pageSize' in event['queryStringParameters']:
                    if int(event['queryStringParameters']['pageSize']) < 1:
                        return proxy_response(400, "Invalid page size provided")
                    else:
                        pageSize = int(event['queryStringParameters']['pageSize'])
                    
                if 'pageIndex' in event['queryStringParameters']:
                    if int(event['queryStringParameters']['pageIndex']) < 1:
                        return proxy_response(400, "Invalid page index provided")
                    else:
                        pageIndex = (int(event['queryStringParameters']['pageIndex']) - 1) * pageSize
                
                if 'searchText' in event['queryStringParameters']:
                    searchText = f"*{event['queryStringParameters']['searchText']}*"
                
                if 'currentLatitude' in event['queryStringParameters'] and 'currentLongitude' in event['queryStringParameters']:
                    currentLatitude = str(event['queryStringParameters']['currentLatitude'])
                    currentLongitude = str(event['queryStringParameters']['currentLongitude'])
            except Exception as e:
                return proxy_response(400, str(e))

        hasLocationData = currentLatitude and currentLongitude
        query = None

        if hasLocationData:
            query = {
                "from":pageIndex,
                "size":pageSize,
                "sort": [
                    {
                        "_geo_distance" : {
                        "location": currentLatitude + ', ' + currentLongitude,
                        "order": "asc",
                        "unit": "km",
                        "ignore_unmapped": "false"
                        },
                        "created_at": {"order": "desc"}
                    }], 
                "query": {
                    "bool": {
                        "must": [
                        {
                            "query_string": {
                                "query": searchText,
                                "fields": ["place_and_address"]
                            }
                        }
                        ],
                        "filter":[
                            {"bool": {"must_not": {"exists": {"field": "deleted_at"}}}},
                            {"bool": {"must_not": {"term":{"filename":""}}}}
                        ]
                    }
                },
                "_source": ["id", "placename", "placeid", "address", "filename", "directory","posttype", "location", "googleplaceid" ],
                "collapse": {
                    "field": "placeid",
                    "inner_hits": {"name": "count", "size": 0}
                },
                "aggs":{
                    "place_count": {
                        "cardinality": {
                            "field": "placeid"
                        }
                    }
                }
            }
        else:
            query = {
                "from":0,
                "size":0,
                "query":{
                    "bool":{
                        "must":[
                            {
                            "query_string":{
                                "query": searchText,
                                "fields":[
                                    "place_and_address"
                                ]
                            }
                            }
                        ],
                        "filter":[
                            {"bool": {"must_not": {"exists": {"field": "deleted_at"}}}},
                            {"bool": {"must_not": {"term":{"filename":""}}}}
                        ]
                    }
                },
                "aggs":{
                    "same_place_id":{
                        "terms":{
                            "field":"placeid",
                            "size": 1000
                        },
                        "aggs":{
                            "most_recent_post":{
                            "top_hits":{
                                "size":1,
                                "sort":[
                                    {
                                        "created_at":{
                                        "order":"desc"
                                        }
                                    }
                                ],
                                "_source":[
                                    "id",
                                    "placename",
                                    "placeid",
                                    "address",
                                    "filename",
                                    "directory",
                                    "posttype",
                                    "location",
                                    "googleplaceid"
                                ]
                            }
                            },
                            "pagination_sort":{
                            "bucket_sort":{
                                "from": pageIndex,
                                "size": pageSize
                            }
                            }
                        }
                    },
                    "place_count":{
                        "cardinality":{
                            "field":"placeid"
                        }
                    }
                }
            }
        
        if (searchText == '*' and not hasLocationData):
            resultCount, rdata = get_cached_result(pageIndex, pageSize)
        else:
            # Elasticsearch connection
            # ES 6.x requires an explicit Content-Type header
            headers = { "Content-Type": "application/json" }
            host = config['es_endpoint']
            index = 'post'
            url = host + '/' + index + '/_search'
            esauth = (config['es_username'], config['es_password'])
            # Make the signed HTTP request
            r = requests.get(url, auth=esauth, headers=headers, data=json.dumps(query)).json()
            # Has more pages
            resultCount = r.get('aggregations').get('place_count').get('value')

        
        hasMorePages = (resultCount - (pageSize + pageIndex)) > 0

        if hasLocationData:
            rdata = r.get('hits').get('hits') 
        elif searchText != "*":
            rdata = r.get('aggregations').get('same_place_id').get('buckets')
        
        responseBody = {}
        responseBody.setdefault("message", "List of places retrived.")
        responseBody.setdefault("status", True)
        responseBody.setdefault("statusCode", 200)
        responseBody.setdefault("hasMorePages", hasMorePages)
        responseBody.setdefault("count", resultCount)
        responseBody.setdefault("data", [])
        
        # Get list of followed places of current user
        following = getFollowingPlaces(currentUserId)
        
        for item in rdata:
            
            place = {}
            if hasLocationData:
                placeData = item.get('_source')
                place["postCount"] = item.get("inner_hits").get("count").get("hits").get("total").get("value")
                place["distance"] = str(item.get('sort')[0])
            else:
                placeData = item.get('most_recent_post').get('hits').get('hits')[0].get('_source')
                place["postCount"] = item.get("doc_count")
                place["distance"] = '0.0'

            place["id"] = placeData.get('placeid')
            place["name"] = placeData.get('placename')
            place["address"] = placeData.get('address') 
            place["googlePlaceId"] = placeData.get('googleplaceid') if 'googleplaceid' in placeData and placeData.get('googleplaceid') else None
            place["url"] = url_builder(placeData.get('directory', ''), placeData.get('filename', ''))

            if(placeData.get('filename') and placeData.get('filename').endswith(('.png', '.jpg', '.jpeg'))):
                place["type"] = 'image'
            elif(placeData.get('filename') and placeData.get('filename').endswith(('.mp4'))):
                place["type"] = 'video'
                
            if(placeData.get('location')):
                    try:
                        place['latitude'] = placeData.get('location').split(', ')[0]
                        place['longitude'] = placeData.get('location').split(', ')[1]
                    except:
                        place['latitude'] = '0.0'
                        place['longitude'] = '0.0'
        
            if placeData.get('placeid') in following:
                place["isFollowing"] = True
            else:
                place["isFollowing"] = False

            responseBody['data'].append(place)


        # Create the response and add some extra content to support CORS
        response = {
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Origin": '*'
            },
            "isBase64Encoded": False
        }

        # Add the search results to the response
        response['body'] = json.dumps(responseBody)

        return response
    
    except Exception as e:
        print(e)
        return {
            "statusCode": 500,
            "headers": {
                "Access-Control-Allow-Origin": '*'
            },
            "isBase64Encoded": False
        }

def getFollowingPlaces(id):

    try:
        key = str(id) + ':follow_places'
        # Check if user id exist in cache
        if(redisInstance.exists(key)):
            # Get from cache if exist
            places = redisInstance.lrange(key, 0, -1)
        else:
            # Get from SQL server if not
            sqlquery = 'SELECT placeId from PlaceFollower where userId = ' + str(id)  + ' AND unfollowDate IS NULL'
            cursor.execute(sqlquery)
            places = [place[0] for place in cursor.fetchall()]

            # If list is empty, cache a 'empty' list
            if(not places):
                places = [0]

            # Put the data in cache
            redisInstance.lpush(key, *places)
            # TTL 5 seconds
            redisInstance.expire(key, 5)

        return places

    except Exception as e:
        raise(e)


def getUserId(token):
    
    # Getting userid from auth gateway
    headers = { "Authorization": token }
    url = config['auth_gateway']
    userid = None

    try:
        auth = requests.get(url, headers=headers).json()
        userid = auth.get('data').get('id')
        return userid
    except Exception as e:
        print('Authorization error ', e)
        return None

def proxy_response(code, message):
    return {
        "statusCode": code,
        "headers": {
            "Access-Control-Allow-Origin": '*'
        },
        "body": json.dumps({
            "message": message
            }),
        "isBase64Encoded": False
    }
    
    
def get_creds():
    ssm_client = boto3.client('ssm')
    try:
        params = ssm_client.get_parameters(
            Names = [os.environ['APP_CONFIG_PATH']],
            WithDecryption = True
        )
        creds = json.loads(params['Parameters'][0]['Value'])
        return creds
    except:
        print("Encountered error trying to get config from SSM")


def init_conn():

    global config
    if(not config):
        # Init creds
        config = get_creds()
        # Init RDS connection
        global cnxn
        cnxn = psycopg2.connect(user = config['sql_id'],
                        password = config['sql_pw'],
                        host = config['sql_connection'],
                        port = "5432",
                        database = config['sql_db_name']
                        )
        global cursor
        cursor = cnxn.cursor()
        # Init Redis connection
        global redisInstance
        redisInstance = redis.Redis(host=config['redis_endpoint'], port=6379, db=0, charset='utf-8', decode_responses=True)
        
        
def get_cached_result(pageIndex, pageSize):
    
    if(redisInstance.exists('place:data')):
          # Get from cache if exist
          count = redisInstance.get('place:count')
          cached_result = redisInstance.lrange('place:data', pageIndex, pageIndex + pageSize - 1)
          hashtags = [json.loads(tag) for tag in cached_result]
          
          return int(count), hashtags
    else:
        return None
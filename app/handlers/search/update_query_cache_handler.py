import os
import boto3
import json
import requests
import redis


config = None
redisInstance = None
def lambda_handler(event, context):
    
    global config
    config = get_creds()
    global redisInstance
    redisInstance = redis.Redis(host=config['redis_endpoint'], port=6379, db=0, charset='utf-8', decode_responses=True)
    
    hashtag_query_result = query_hashtag_wildcard()
    cache_response = cache_result('hashtag', hashtag_query_result["count"], hashtag_query_result["data"])
    
    place_query_result = query_places_wildcard()
    cache_response = cache_result('place', place_query_result["count"], place_query_result["data"])
      
def query_hashtag_wildcard():
    
    query = {
        "from":0,
        "size":0,
        "query":{
            "bool":{
              "must":[
                  {
                    "query_string":{
                        "query": "*",
                        "fields":[
                          "name"
                        ]
                    }
                  }
              ],
              "filter": [
                    {
                        "bool": {"must_not": {"term":{"name.keyword":""}}}
                    },
                    {
                        "bool": {"must_not": {"exists": {"field": "deleted_at"}}}
                    }
                ]
            }
        },
        "aggs":{
            "group_by_hashtags":{
              "terms":{
                  "field":"name.keyword",
                  "size": 1000
              },
              "aggs":{
                  "latest_post":{
                    "top_hits":{
                        "size":1,
                        "sort":[
                          {
                              "created_at":{
                                "order":"desc"
                              }
                          }
                        ],
                        "_source":[
                          "name",
                          "type",
                          "directory",
                          "filename"
                        ]
                    }
                  },
                  "pagination_sort":{
                    "bucket_sort":{
                        "from":0,
                        "size":1000
                    }
                  }
              }
            },
            "hashtag_count":{
              "cardinality":{
                  "field":"name.keyword"
              }
            }
        }
    }
    
    # Elasticsearch connection
    # ES 6.x requires an explicit Content-Type header
    headers = { "Content-Type": "application/json" }
    host = config['es_endpoint']
    index = 'hashtag'
    url = host + '/' + index + '/_search'
    esauth = (config['es_username'], config['es_password'])
    # Make the signed HTTP request
    r = requests.get(url, auth=esauth, headers=headers, data=json.dumps(query)).json()
    
    rdata = r.get('aggregations').get('group_by_hashtags').get('buckets')
    count = r.get('aggregations').get('hashtag_count').get('value')
    
    return {
        "count": count,
        "data": rdata
    }
    
    
    
def query_places_wildcard():
    query = {
        "from":0,
        "size":0,
        "query":{
            "bool":{
                "must":[
                    {
                    "query_string":{
                        "query": "*",
                        "fields":[
                            "place_and_address"
                        ]
                    }
                    }
                ],
                "filter":[
                    {"bool": {"must_not": {"exists": {"field": "deleted_at"}}}},
                    {"bool": {"must_not": {"term":{"filename":""}}}}
                ]
            }
        },
        "aggs":{
            "same_place_id":{
                "terms":{
                    "field":"placeid",
                    "size": 1000
                },
                "aggs":{
                    "most_recent_post":{
                    "top_hits":{
                        "size":1,
                        "sort":[
                            {
                                "created_at":{
                                "order":"desc"
                                }
                            }
                        ],
                        "_source":[
                            "id",
                            "placename",
                            "placeid",
                            "address",
                            "filename",
                            "directory",
                            "posttype",
                            "location",
                            "googleplaceid"
                        ]
                    }
                    },
                    "pagination_sort":{
                    "bucket_sort":{
                        "from": 0,
                        "size": 1000
                    }
                    }
                }
            },
            "place_count":{
                "cardinality":{
                    "field":"placeid"
                }
            }
        }
    }
    
    # ES 6.x requires an explicit Content-Type header
    headers = { "Content-Type": "application/json" }
    host = config['es_endpoint']
    index = 'post'
    url = host + '/' + index + '/_search'
    esauth = (config['es_username'], config['es_password'])
    # Make the signed HTTP request
    r = requests.get(url, auth=esauth, headers=headers, data=json.dumps(query)).json()
    
    count = r.get('aggregations').get('place_count').get('value')
    rdata = r.get('aggregations').get('same_place_id').get('buckets')
    
    return {
        "count": count,
        "data": rdata
    }

def get_creds():
    ssm_client = boto3.client('ssm')
    try:
        params = ssm_client.get_parameters(
            Names = [os.environ['APP_CONFIG_PATH']],
            WithDecryption = True
        )
        creds = json.loads(params['Parameters'][0]['Value'])
        return creds
    except:
        print("Encountered error trying to get config from SSM")
        
        
def cache_result(name, count, data):
    
    redisInstance.delete(name + ':count')
    redisInstance.delete(name + ':data')
    
    
    redisInstance.mset(({ name + ":count": count}))
    for item in data:
        redisInstance.rpush(name + ':data', json.dumps(item))
    
    print("Cached data for ", name)
    
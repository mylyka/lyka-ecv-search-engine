import os
import boto3
import json
import requests
import psycopg2
import redis
from app.util.url_utils import url_builder

# Store related backend credentials
config = None
# Postgresql connection
cnxn = None
cursor = None
# Redis connection
redisInstance = None
             
def lambda_handler(event, context):

    print(event)

    # Configure connections if does not exist
    init_conn()

    try:

      currentUserId = None
    
      if 'headers' in event and 'Authorization' in event['headers']:
          currentUserId = getUserId(event['headers']['Authorization'])
      
      if currentUserId is None:
            response = {
                "statusCode": 401,
                "headers": {
                    "Access-Control-Allow-Origin": '*'
                },
                "isBase64Encoded": False
            }
            return response
    
      # Default pagination values
      pageSize = 10
      pageIndex = 0
      searchText = '*'
      
      # Building query according to query string params
      if 'queryStringParameters' in event and event['queryStringParameters'] is not None:

        try:
            if 'pageSize' in event['queryStringParameters']:
                    if int(event['queryStringParameters']['pageSize']) < 1:
                        return proxy_response(400, "Invalid page size provided")
                    else:
                        pageSize = int(event['queryStringParameters']['pageSize'])

            if 'pageIndex' in event['queryStringParameters']:
                if int(event['queryStringParameters']['pageIndex']) < 1:
                    return proxy_response(400, "Invalid page index provided")
                else:
                    pageIndex = (int(event['queryStringParameters']['pageIndex']) - 1) * pageSize
        
            if 'searchText' in event['queryStringParameters']:
                searchText = f"{event['queryStringParameters']['searchText']}*"
        except Exception as e:
            return proxy_response(400, str(e))

      # Put the user query into the query DSL for more accurate search results.
      # Note that certain fields are boosted (^).
      query = {
        "from":0,
        "size":0,
        "query":{
            "bool":{
              "must":[
                  {
                    "query_string":{
                        "query": searchText,
                        "fields":[
                          "name"
                        ]
                    }
                  }
              ],
              "filter": [
                    {
                        "bool": {"must_not": {"term":{"name":""}}}
                    },
                    {
                        "bool": {"must_not": {"exists": {"field": "deleted_at"}}}
                    }
                ]
            }
        },
        "aggs":{
            "group_by_hashtags":{
              "terms":{
                  "field":"name.keyword",
                  "size": 1000
              },
              "aggs":{
                  "latest_post":{
                    "top_hits":{
                        "size":1,
                        "sort":[
                          {
                              "created_at":{
                                "order":"desc"
                              }
                          }
                        ],
                        "_source":[
                          "name",
                          "type",
                          "directory",
                          "filename"
                        ]
                    }
                  },
                  "pagination_sort":{
                    "bucket_sort":{
                        "from":pageIndex,
                        "size":pageSize
                    }
                  }
              }
            },
            "hashtag_count":{
              "cardinality":{
                  "field":"name.keyword"
              }
            }
        }
      }

      # Elasticsearch connection
      # ES 6.x requires an explicit Content-Type header
      headers = { "Content-Type": "application/json" }
      host = config['es_endpoint']
      index = 'hashtag'
      url = host + '/' + index + '/_search'
      esauth = (config['es_username'], config['es_password'])
      
      if(searchText == '*'):
          resultCount, rdata = get_cached_result(pageIndex, pageSize)
      elif(len(searchText) < 4):
          rdata = []
          resultCount = 0
      else:
          # Make the signed HTTP request
          r = requests.get(url, auth=esauth, headers=headers, data=json.dumps(query)).json()
          # Result of most recent relevent hashtag
          rdata = r.get('aggregations').get('group_by_hashtags').get('buckets')
          # Has more pages
          resultCount = r.get('aggregations').get('hashtag_count').get('value')
      
     
      hasMorePages = (resultCount - (pageIndex + pageSize)) > 0

      responseBody = {}
      responseBody.setdefault("message", "List of hashtags retrived.")
      responseBody.setdefault("status", True)
      responseBody.setdefault("statusCode", 200)
      responseBody.setdefault("hasMorePages", hasMorePages)
      responseBody.setdefault("count", resultCount)
      responseBody.setdefault("data", [])
      
      # Get list of followed users of current user
      followingTags = getFollowingHashtag(currentUserId) 
      

      for post in rdata:
          hashtag = {}
          postData = post.get('latest_post').get('hits').get('hits')[0].get('_source')

          hashtag["name"] = postData.get('name')
          hashtag["type"] = postData.get('type')
          hashtag["url"] = url_builder(postData.get('directory', ''), postData.get('filename', ''))
          hashtag["postCount"] = post.get('doc_count')

          if(postData.get('filename') and postData.get('filename').endswith(('.png', '.jpg', '.jpeg'))):
              hashtag["type"] = 'image'
          elif(postData.get('filename') and postData.get('filename').endswith(('.mp4'))):
              hashtag["type"] = 'video'
          
          if postData.get('name') in followingTags:
              hashtag["isFollowing"] = True
          else:
              hashtag["isFollowing"] = False
          
          responseBody['data'].append(hashtag)


      # Create the response and add some extra content to support CORS
      response = {
          "statusCode": 200,
          "headers": {
              "Access-Control-Allow-Origin": '*'
          },
          "isBase64Encoded": False
      }

      # Add the search results to the response
      response['body'] = json.dumps(responseBody)

      return response

    except Exception as e:
      print(e)
      return {
            "statusCode": 500,
            "headers": {
                "Access-Control-Allow-Origin": '*'
            },
            "isBase64Encoded": False
        }


def getFollowingHashtag(id):
    try:
      key = str(id) + ':follow_hashtag'
      # Check if user id exist in cache
      if(redisInstance.exists(key)):
          # Get from cache if exist
          tags = redisInstance.lrange(key, 0, -1)
      else:
          # Get from SQL server if not
          sqlquery = 'SELECT Name FROM HashtagFollower WHERE UserId = '+ str(id) + ' AND IsUnfollowed = 0'
          cursor.execute(sqlquery)
          tags = [tag[0] for tag in cursor.fetchall()]

          # If list is empty, cache a 'empty' list
          if(not tags):
              tags = [0]

          # Put the data in cache
          redisInstance.lpush(key, *tags)
          # TTL 5 seconds
          redisInstance.expire(key, 5)
    
    except Exception as e:
      raise(e)

    return tags

def getUserId(token):
    
    # Getting userid from auth gateway
    headers = { "Authorization": token }
    url = config['auth_gateway']
    userid = None

    try:
        auth = requests.get(url, headers=headers).json()
        userid = auth.get('data').get('id')
        return userid
    except Exception as e:
        print('Authorization error ', e)
        return None

def proxy_response(code, message):
    return {
        "statusCode": code,
        "headers": {
            "Access-Control-Allow-Origin": '*'
        },
        "body": json.dumps({
            "message": message
            }),
        "isBase64Encoded": False
    }

def get_creds():
    ssm_client = boto3.client('ssm')
    try:
        params = ssm_client.get_parameters(
            Names = [os.environ['APP_CONFIG_PATH']],
            WithDecryption = True
        )
        creds = json.loads(params['Parameters'][0]['Value'])
        return creds
    except:
        print("Encountered error trying to get config from SSM")


def init_conn():

    global config
    if(not config):
        # Init creds
        config = get_creds()
        # Init RDS connection
        global cnxn
        cnxn = psycopg2.connect(user = config['sql_id'],
                        password = config['sql_pw'],
                        host = config['sql_connection'],
                        port = "5432",
                        database = config['sql_db_name']
                        )
        global cursor
        cursor = cnxn.cursor()
        # Init Redis connection
        global redisInstance
        redisInstance = redis.Redis(host=config['redis_endpoint'], port=6379, db=0, charset='utf-8', decode_responses=True)
        
def get_cached_result(pageIndex, pageSize):
    
    if(redisInstance.exists('hashtag:data')):
          # Get from cache if exist
          count = redisInstance.get('hashtag:count')
          cached_result = redisInstance.lrange('hashtag:data', pageIndex, pageIndex + pageSize - 1)
          hashtags = [json.loads(tag) for tag in cached_result]
          
          return int(count), hashtags
    else:
        return 0, []
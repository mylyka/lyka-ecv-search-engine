import os
import boto3
import json
import requests
import psycopg2
import redis
from app.util.url_utils import url_builder

# Store related backend credentials with execution context
config = None
# Postgresql connection
cnxn = None
cursor = None
# Redis connection
redisInstance = None

# Lambda execution starts here
def lambda_handler(event, context):

    print(event)
    
    # Configure connections if does not exist
    init_conn()

    try:
        currentUserId = None
        
        if 'headers' in event and 'Authorization' in event['headers']:
            currentUserId = getUserId(event['headers']['Authorization'])
        
        if currentUserId is None:
            response = {
                "statusCode": 401,
                "headers": {
                    "Access-Control-Allow-Origin": '*'
                },
                "isBase64Encoded": False
            }
            return response
        
        # Default pagination values
        pageSize = 10
        pageIndex = 0
        searchText = '*'
        currentLatitude = None
        currentLongitude = None
        
        # Building query according to query string params
        if 'queryStringParameters' in event and event['queryStringParameters'] is not None:

            try:
                if 'pageSize' in event['queryStringParameters']:
                    if int(event['queryStringParameters']['pageSize']) < 1:
                        return proxy_response(400, "Invalid page size provided")
                    else:
                        pageSize = int(event['queryStringParameters']['pageSize'])
                    
                if 'pageIndex' in event['queryStringParameters']:
                    if int(event['queryStringParameters']['pageIndex']) < 1:
                        return proxy_response(400, "Invalid page index provided")
                    else:
                        pageIndex = (int(event['queryStringParameters']['pageIndex']) - 1) * pageSize
                
                if 'searchText' in event['queryStringParameters']:
                    searchText = f"*{event['queryStringParameters']['searchText']}*"
                
                if 'currentLatitude' in event['queryStringParameters'] and 'currentLongitude' in event['queryStringParameters']:
                    currentLatitude = str(event['queryStringParameters']['currentLatitude'])
                    currentLongitude = str(event['queryStringParameters']['currentLongitude'])
            except Exception as e:
                return proxy_response(400, str(e))
            
        hasLocationData = currentLatitude and currentLongitude
        query = None

        if hasLocationData:
            query = {
                "from": pageIndex,
                "size": pageSize,
                "sort":[
                    {
                        "isVerified": {"order": "desc"}
                    },
                    {
                        "_geo_distance" : {
                        "location": currentLatitude + ', ' + currentLongitude,
                        "order": "asc",
                        "unit": "km",
                        "ignore_unmapped": "true"
                        }
                    },{
                        "userName": {"order": "asc"}
                    }
                ],
                "query": {
                    "bool": {
                        "must": [
                            {
                                "query_string": {
                                    "query": searchText,
                                    "fields": ["userName", "fullName", "description", "full_address"]
                                }
                            }
                        ],
                        "filter": [
                            {
                                "term": {"isMerchant": True}
                            },
                            {
                                "bool": {"must_not": {"exists": {"field": "deleted_at"}}}
                            }
                        ]
                    }
                },
                "_source": ["id", "userName", "fullName", "description", "directory", "filename", "isMerchant", "fbId", "isVerified", "location", "street_address", "country", "city", "state"]
            }
        else:
            query = {
                "from": pageIndex,
                "size": pageSize,
                "sort":[
                    {
                        "isVerified": {"order": "desc"},
                        "userName": {"order": "asc"}
                    }],
                "query": {
                    "bool": {
                        "must": [
                            {
                                "query_string": {
                                    "query": searchText,
                                    "fields": ["userName", "fullName", "description", "full_address"]
                                }
                            }
                        ],
                        "filter": [
                            {
                                "term": {"isMerchant": True}
                            },
                            {
                                "bool": {"must_not": {"exists": {"field": "deleted_at"}}}
                            }
                        ]
                    }
                },
                "_source": ["id", "userName", "fullName", "description", "directory", "filename", "isMerchant", "fbId", "isVerified", "location", "street_address", "country", "city", "state"]
            }

        
        # Elasticsearch connection
        # ES 6.x requires an explicit Content-Type header
        headers = { "Content-Type": "application/json" }
        host = config['es_endpoint']
        index = 'user'
        url = host + '/' + index + '/_search'
        esauth = (config['es_username'], config['es_password'])
        # Make the signed HTTP request
        r = requests.get(url, auth=esauth, headers=headers, data=json.dumps(query)).json()
        
        # Result of matched users
        rdata = r.get('hits').get('hits') 
        
        # Has more pages
        resultCount = r.get('hits').get('total').get('value')
        hasMorePages = (resultCount - (pageSize + pageIndex)) > 0
        
        responseBody = {}
        responseBody.setdefault("message", "List of merchants retrived.")
        responseBody.setdefault("status", True)
        responseBody.setdefault("statusCode", 200)
        responseBody.setdefault("hasMorePages", hasMorePages)
        responseBody.setdefault("count", resultCount)
        responseBody.setdefault("data", [])
        
        # Get list of followed/blacklist users of current user
        following = getUserList(currentUserId, 'following')
        blacklist = getUserList(currentUserId, 'blacklist')


        for item in rdata:

            user = {}
            userData = item.get('_source')

            if(userData.get('id') not in blacklist):
                user["id"] = userData.get('id')
                user["fullName"] = userData.get('fullName')
                user["userName"] = userData.get('userName')
                user["fbId"] = str(userData.get('fbId')) if 'fbId' in userData and userData.get('fbId') else None
                user["isVerified"] = userData.get('isVerified')
                user["isMerchant"] = userData.get('isMerchant')
                user["profilePictureUrl"] = url_builder(userData.get('directory', ''), userData.get('filename', ''))
                user["address"] = ', '.join(filter(None, [userData.get('street_address'), userData.get('city'), userData.get('state'), userData.get('country')]))
                user["description"] = userData.get('description')

                if hasLocationData:
                    user["distance"] = str(item.get('sort')[1])
                else:
                    user["distance"] = '0.0'

                if(userData.get('location')):
                    try:
                        user['latitude'] = userData.get('location').split(', ')[0]
                        user['longitude'] = userData.get('location').split(', ')[1]
                    except:
                        user['latitude'] = '0.0'
                        user['longitude'] ='0.0'

                if(userData.get('id') in following):
                    user['isFollowing'] = True
                else:
                    user['isFollowing'] = False 

                responseBody['data'].append(user)
    

        # Create the response and add some extra content to support CORS
        response = {
            "statusCode": 200,
            "headers": {
                "Access-Control-Allow-Origin": '*'
            },
            "isBase64Encoded": False
        }

        # Add the search results to the response
        response['body'] = json.dumps(responseBody)

        return response

    except Exception as e:
        print(e)
        return{
            "statusCode": 500,
            "headers": {
                "Access-Control-Allow-Origin": '*'
            },
            "isBase64Encoded": False
        }

def getUserList(id, type):
    sqlquery = ''
    if(type == 'following'):
        key = str(id) + ':following'
        sqlquery = 'SELECT user_id from follower where follower_id = ' + str(id)  + 'AND is_unfollowed = 0'
    elif(type == 'blacklist'):
        key = str(id) + ':blacklist'
        sqlquery = 'SELECT reference_id from blacklist where user_id = ' + str(id) + 'AND deleted_at IS NULL'

    try:
        # Check if user id exist in cache
        if(redisInstance.exists(key)):
            # Get from cache if exist
            userList = redisInstance.lrange(key, 0, -1)
        else:
            # Get from SQL server if not
            cursor.execute(sqlquery)
            userList = [user[0] for user in cursor.fetchall()]

            # If list is empty, cache a 'empty' list
            if(not userList):
                userList = [0]

            # Put the data in cache
            redisInstance.lpush(key, *userList)
            # TTL 5 seconds
            redisInstance.expire(key, 5)

        return userList
    
    except Exception as e:
        raise(e)

def getUserId(token):
    
    # Getting userid from auth gateway
    headers = { "Authorization": token }
    url = config['auth_gateway']
    userid = None

    try:
        auth = requests.get(url, headers=headers).json()
        userid = auth.get('data').get('id')
        return userid
    except Exception as e:
        print('Authorization error ', e)
        return None

def proxy_response(code, message):
    return {
        "statusCode": code,
        "headers": {
            "Access-Control-Allow-Origin": '*'
        },
        "body": json.dumps({
            "message": message
            }),
        "isBase64Encoded": False
    }
    
def get_creds():
    ssm_client = boto3.client('ssm')
    try:
        params = ssm_client.get_parameters(
            Names = [os.environ['APP_CONFIG_PATH']],
            WithDecryption = True
        )
        creds = json.loads(params['Parameters'][0]['Value'])
        return creds
    except:
        print("Encountered error trying to get config from SSM")


def init_conn():
    
    global config
    if(not config):
        # Init creds
        config = get_creds()
        # Init RDS connection
        global cnxn
        cnxn = psycopg2.connect(user = config['sql_id'],
                        password = config['sql_pw'],
                        host = config['sql_connection'],
                        port = "5432",
                        database = config['sql_db_name']
                        )
        global cursor
        cursor = cnxn.cursor()
        # Init Redis connection
        global redisInstance
        redisInstance = redis.Redis(host=config['redis_endpoint'], port=6379, db=0, charset='utf-8', decode_responses=True)

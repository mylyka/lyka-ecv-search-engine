# Usage
1. Update `-sql_last_value.yml` file under `postgres_queries/` folder to offset skipped incremental index in database.
2. Update logstash execution script (if on Windows) under logstash installation location, add the following to the end of `logstash.bat` otherwise the loop script will not work properly:
```bat
exit 0
```
3. Start ingestion by running `-ingest-loop.bat` file to start the ingestion.
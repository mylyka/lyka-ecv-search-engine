SELECT HT.id as hashtag_id
		  ,lower(HT.name) as name
		  ,HT.createdate as created_at
		  ,HT.deletedate as deleted_at
		  ,WI.id as wishlist_image_id
		  ,WI.directory
		  ,WI.filename
		  ,WI.type
		  ,WI.wishlist_id
		  ,CONCAT(HT.id, WI.id) AS id
	  FROM Hashtag HT
	  LEFT OUTER JOIN wishlist_image WI
	  ON HT.ReferenceId = WI.wishlist_id
	  WHERE HT.id > ? AND HT.id < ? + ?
	  ORDER BY HT.id ASC

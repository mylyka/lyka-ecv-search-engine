SELECT PS.id
	  ,PS.directory
	  ,PS.filename
	  ,PS.created_at
	  ,PS.updated_at
	  ,PS.deleted_at
	  ,PP.placeid
	  ,PL.Name AS placename
	  ,PL.Address
	  ,COALESCE(CAST(PL.Latitude AS varchar(255)), 'null') AS latitude
	  ,COALESCE(CAST(PL.Longitude AS varchar(255)), 'null') AS longitude
	  ,PL.GooglePlaceId
	FROM wishlist PS
	LEFT OUTER JOIN PostPlace PP
	ON (PS.id = PP.referenceid)
	LEFT OUTER JOIN Place PL
	ON (PP.placeid = PL.id)
	WHERE PS.id > ? AND PS.id < ? + ?
	ORDER BY PS.id ASC
	

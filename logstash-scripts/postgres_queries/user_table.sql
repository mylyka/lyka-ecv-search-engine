SELECT id
      ,name
      ,username
      ,description
      ,fb_id
      ,directory
      ,filename
      ,created_at
      ,updated_at
      ,deleted_at
      ,country
      ,state
      ,city
      ,street_address
      ,verified
      ,is_mearchant
      ,COALESCE(latitude, 'null') AS latitude
      ,COALESCE(longitude, 'null') AS longitude
      ,google_place_id
    FROM "user"
    WHERE id > ? AND id < ? + ?
    ORDER BY id ASC

service: lyka-search-serverless
# app and org for use with dashboard.serverless.com
#app: your-app-name
#org: your-org-name

# you can add packaging information here
package:
  exclude:
    - es-queries-sample/**
    - index_setup/**
    - lambda-input/**
    - lambda-search/**
    - lib/**
    - logstash-scripts/**
    - mappings/**
    - postman/**
    - lyka-search-swagger-specification.yaml
    - requirements.txt
    - README.md
    - app/config.json
    - app/config-dev.json
    - creds.yml
    - config.json
    - serverless-dev.yml
    - serverless-uat.yml
    - serverless.yml


  include:
    - app/**
    
# You can pin your service to only deploy with a specific Serverless version
# Check out our docs for more details
frameworkVersion: '2'

provider:
  name: aws
  runtime: python3.8
  vpc:
    securityGroupIds:
      - sg-044b173fc92194cdc #LykaSearchProd-env SG
    subnetIds:
      - subnet-03641d3dc85105a75 #Prod-PrivateSubnet1A
      - subnet-0e4c5e75e0e49ce98 #Prod-PrivateSubnet1B
      - subnet-014162eb092f70ac7 #Prod-PrivateSubnet1C

# you can overwrite defaults here
  stage: prod
  region: ap-southeast-1

# you can add statements to the Lambda function's IAM Role here
  iamRoleStatements:
    - Effect: "Allow"
      Action:
        - "es:*"
      Resource: "*"
    - Effect: "Allow"
      Action:
        - "sqs:*"
      Resource: 
        - !GetAtt IngestUserSQSQueue.Arn
        - !GetAtt IngestPostSQSQueue.Arn
        - !GetAtt DeleteUserSQSQueue.Arn
        - !GetAtt DeletePostSQSQueue.Arn
    - Effect: "Allow"
      Action:
        - "elasticache:*"
      Resource: "*"
    - Effect: "Allow"
      Action:
        - "ssm:*"
      Resource: "*"

# you can define service wide environment variables here
  environment:
    APP_CONFIG_PATH: /prod/search-backend-config.json

layers:
  redis:
    path: ./lib

functions:
  SearchUserHandler:
    handler: app/handlers/search/search_user_handler.lambda_handler
    events:
      - http:
          path: /user
          method: get
    layers:
      - {Ref: RedisLambdaLayer}
      - arn:aws:lambda:ap-southeast-1:898466741470:layer:psycopg2-py38:1
      
  SearchMerchantHandler:
    handler: app/handlers/search/search_merchant_handler.lambda_handler
    events:
      - http:
          path: /merchant
          method: get
    layers:
      - {Ref: RedisLambdaLayer}
      - arn:aws:lambda:ap-southeast-1:898466741470:layer:psycopg2-py38:1

  SearchHashtagHandler:
    handler: app/handlers/search/search_hashtag_handler.lambda_handler
    events:
      - http:
          path: /hashtag
          method: get
    layers:
      - {Ref: RedisLambdaLayer}
      - arn:aws:lambda:ap-southeast-1:898466741470:layer:psycopg2-py38:1

  SearchPlaceHandler:
    handler: app/handlers/search/search_place_handler.lambda_handler
    events:
      - http:
          path: /place
          method: get
    layers:
      - {Ref: RedisLambdaLayer}
      - arn:aws:lambda:ap-southeast-1:898466741470:layer:psycopg2-py38:1

  IngestUserHandler:
    handler: app/handlers/ingest/ingest_user_handler.lambda_handler
    events:
      - http:
          path: /user
          method: post
    environment:
      SQS_QUEUE_URL:
        Ref: IngestUserSQSQueue
      SQS_VPC_INTERFACE_ENDPOINT:
        !Join [ "", [ "https://", !Select [1, !Split [ ":", !Select [ 0, !GetAtt VPCInterfaceEndpointSQS.DnsEntries ]]]]]

  IngestPostHandler:
    handler: app/handlers/ingest/ingest_post_handler.lambda_handler
    events:
      - http:
          path: /post
          method: post
    environment:
      SQS_QUEUE_URL:
        Ref: IngestPostSQSQueue
      SQS_VPC_INTERFACE_ENDPOINT:
        !Join [ "", [ "https://", !Select [1, !Split [ ":", !Select [ 0, !GetAtt VPCInterfaceEndpointSQS.DnsEntries ]]]]]

  DeleteUserHandler:
    handler: app/handlers/delete/delete_user_handler.lambda_handler
    events:
      - http:
          path: /user/delete
          method: post
    environment:
      SQS_QUEUE_URL:
        Ref: DeleteUserSQSQueue
      SQS_VPC_INTERFACE_ENDPOINT:
        !Join [ "", [ "https://", !Select [1, !Split [ ":", !Select [ 0, !GetAtt VPCInterfaceEndpointSQS.DnsEntries ]]]]]

  DeletePostHandler:
    handler: app/handlers/delete/delete_post_handler.lambda_handler
    events:
      - http:
          path: /post/delete
          method: post
    environment:
      SQS_QUEUE_URL:
        Ref: DeletePostSQSQueue
      SQS_VPC_INTERFACE_ENDPOINT:
        !Join [ "", [ "https://", !Select [1, !Split [ ":", !Select [ 0, !GetAtt VPCInterfaceEndpointSQS.DnsEntries ]]]]]

  IngestUserSQSTrigger:
    handler: app/queue/user_mapping_sqs_trigger.lambda_handler
    events:
      - sqs:
          arn:
            Fn::GetAtt:
              - IngestUserSQSQueue
              - Arn
    layers:
      - arn:aws:lambda:ap-southeast-1:770693421928:layer:Klayers-python38-elasticsearch:17

  IngestPostSQSTrigger:
    handler: app/queue/post_mapping_sqs_trigger.lambda_handler
    events:
      - sqs:
          arn:
            Fn::GetAtt:
              - IngestPostSQSQueue
              - Arn
    layers:
      - arn:aws:lambda:ap-southeast-1:770693421928:layer:Klayers-python38-elasticsearch:17

  DeleteUserSQSTrigger:
    handler: app/queue/delete_user_sqs_trigger.lambda_handler
    events:
      - sqs:
          arn:
            Fn::GetAtt:
              - DeleteUserSQSQueue
              - Arn
    layers:
      - arn:aws:lambda:ap-southeast-1:770693421928:layer:Klayers-python38-elasticsearch:17

  DeletePostSQSTrigger:
    handler: app/queue/delete_post_sqs_trigger.lambda_handler
    events:
      - sqs:
          arn:
            Fn::GetAtt:
              - DeletePostSQSQueue
              - Arn
    layers:
      - arn:aws:lambda:ap-southeast-1:770693421928:layer:Klayers-python38-elasticsearch:17
      
  TestDeployment:
    handler: app/test.lambda_handler
    layers:
      - {Ref: RedisLambdaLayer}
      - arn:aws:lambda:ap-southeast-1:898466741470:layer:psycopg2-py38:1
      
  CacheQueryHandler:
    timeout: 60
    handler: app/handlers/search/update_query_cache_handler.lambda_handler
    layers:
      - {Ref: RedisLambdaLayer}
    events:
      - schedule: cron(0 18 * * ? *)
  

# you can add CloudFormation resource templates here
resources:
 Resources:
  IngestUserSQSQueue:
    Type: AWS::SQS::Queue
    Properties:
      QueueName: User_Ingestion_ElasticSearch

  IngestPostSQSQueue:
    Type: AWS::SQS::Queue
    Properties:
      QueueName: Post_Ingestion_ElasticSearch

  DeleteUserSQSQueue:
    Type: AWS::SQS::Queue
    Properties:
      QueueName: User_Deletion_ElasticSearch

  DeletePostSQSQueue:
    Type: AWS::SQS::Queue
    Properties:
      QueueName: Post_Deletion_ElasticSearch

  VPCInterfaceEndpointSQS:
    Type: AWS::EC2::VPCEndpoint
    Properties:
      ServiceName: com.amazonaws.ap-southeast-1.sqs
      VpcEndpointType: Interface
      VpcId: vpc-02525507a146c9774 #Prod-VPC
      SecurityGroupIds:
        - sg-044b173fc92194cdc #LykaSearchProd-env SG
      SubnetIds:
        - subnet-03641d3dc85105a75 #Prod-PrivateSubnet1A
        - subnet-0e4c5e75e0e49ce98 #Prod-PrivateSubnet1B
        - subnet-014162eb092f70ac7 #Prod-PrivateSubnet1C
